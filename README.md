# Задание

- Создаем новый проект в GitLab.
- Создаем новую ветку
- Копируем туда код от домашнего задания по TeamCity
- Копируем к себе файлы
  - `docker-entrypoint.sh`
  - `Dockerfile`
  - `.gitlab-ci.yml`
- Коммитимся
- Отправляем на удаленный сервер `git push --all -u`
- Идем в интерфейс и смотрим результат CI
- Открываем Merge Request в Master
- При "зеленом" CI принимаем его
- Тегируем ветку мастер
- Проверяем образ с тегом и latest в regestry

Для того чтобы нельзя было принять merge request до успешного прохождения CI, нужно в настройках проекта в GitLab включить General Settings -> Merge requests -> Merge checks -> Pipelines must succeed